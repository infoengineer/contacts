# -*- coding: utf-8 -*-

import sys

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtSql import QSqlDatabase, QSqlQuery, QSqlTableModel
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox, QTableView, QAction  
 
def createConnection():
    db = QSqlDatabase.addDatabase('QSQLITE')
    db.setDatabaseName('contacts.db')
    if not db.open():
        QMessageBox.critical(None, "Cannot open database",
                "Unable to establish a database connection.\n"
                "This example needs SQLite support. Please read the Qt SQL "
                "driver documentation for information how to build it.\n\n"
                "Click Cancel to exit.",
                QMessageBox.Cancel)
        return False
    query = QSqlQuery()
    query.exec_("Create table if not exists contacts("
                "firstname varchar(50), lastname varchar(50),"
                "address varchar(200), phone varchar(50),"
                "email varchar(200), website varchar(200))")
    return True

def initializeModel(model):
    model.setTable('contacts')    
    model.setSort(0, Qt.AscendingOrder)
    model.setSort(1, Qt.AscendingOrder)
    model.setEditStrategy(QSqlTableModel.OnFieldChange)
    model.select()    

    model.setHeaderData(0, Qt.Horizontal, "First name")
    model.setHeaderData(1, Qt.Horizontal, "Last name")
    model.setHeaderData(2, Qt.Horizontal, "Address")
    model.setHeaderData(3, Qt.Horizontal, "Phone")
    model.setHeaderData(4, Qt.Horizontal, "Email")
    model.setHeaderData(5, Qt.Horizontal, "Website url")

def createView(model):
    view = QTableView()
    view.setModel(model)
    return view

def addrow():
    model.insertRow(model.rowCount())

def delrow():
    model.removeRow(view.currentIndex().row())
    model.select()

def refresh():
    model.select()

class App(QMainWindow):

    def __init__(self):
        super().__init__()
        self.title = 'Contacts'
        self.width = self.frameGeometry().width()
        self.height = self.frameGeometry().height()
        self.initUI()

    def initUI(self):

        self.setCentralWidget(view)
        desktop = QApplication.desktop()
        screenWidth = desktop.screen().width()
        screenHeight = desktop.screen().height()
        self.setGeometry((screenWidth/2)-(self.width/2), (screenHeight/2)-(self.height/2), self.width, self.height)

        self.setWindowTitle(self.title)
        self.setWindowIcon(QIcon('contacts.png'))
        self.statusBar().showMessage('Ready')

        newAction = QAction(QIcon('add.png'), '&Add', self)
        newAction.setShortcut('Ins')
        newAction.setStatusTip('New record')
        newAction.triggered.connect(addrow)
        delAction = QAction(QIcon('delete.png'), '&Delete', self)
        delAction.setShortcut('Del')
        delAction.setStatusTip('Delete record')
        delAction.triggered.connect(delrow)
        refreshAction = QAction(QIcon('refresh.png'), '&Refresh', self)
        refreshAction.setShortcut('F5')
        refreshAction.setStatusTip('Refresh')
        refreshAction.triggered.connect(refresh)
        exitAction = QAction(QIcon('exit.png'), '&Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.close)
        
        self.menubar = self.menuBar()
        self.fileMenu = self.menubar.addMenu('&Database')
        self.fileMenu.addAction(refreshAction)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(exitAction)
        self.fileMenu = self.menubar.addMenu('&Record')
        self.fileMenu.addAction(newAction)
        self.fileMenu.addAction(delAction)
        
        self.toolbar = self.addToolBar('Contacts')
        self.toolbar.addAction(refreshAction)
        self.toolbar.addSeparator()
        self.toolbar.addAction(newAction)
        self.toolbar.addAction(delAction)
        self.toolbar.addSeparator()
        self.toolbar.addAction(exitAction)

        self.show()

if __name__ == '__main__':

    app = QApplication(sys.argv)

    if not createConnection():
        sys.exit(1)
 
    model = QSqlTableModel()
    view = createView(model)
    initializeModel(model)
    ex = App()
    
    sys.exit(app.exec_())